const products = require('../models/products.js')

const add = async (req,res) => {
    const {catID,color,price,product} = req.body
    try{
        const response = await products.create({catID:catID,color:color,price:price,product:product})
        console.log(response)
        res.send({ok:true, message:`${response.product} created in DB`})
        } catch(error) {
        res.send({ok:false, message:`something went wrong`})
    }
}

const remove = async(req,res) => {
    try{
        const {product} = req.body
        const response = await products.deleteOne({product:product})
        console.log(products,response)
        res.send({ok:true, message:`product: ${product} deleted`})
        } catch {
        res.send({ok:false, message:`something went wrong`})  
    }
}

const displayProds = async(req,res) => {
    try{
        const response= await products.find()
        res.send({ok:true, message:response})
            } catch {
        res.send({ok:false, message:`something went wrong`})  
    }
}    

const getProduct = async(req,res) => {
    const { product, _id } = req.body
    const key = product || _id
        try{
            const response = await products.findOne({[product ? "product":"_id"]:key})
            res.send({ok:true,message:response})
        } catch {
            res.send({ok:false, message:response})  
        }
}

const getProductPage = async(req,res) => {
    const { product, _id } = req.body
    const key = product || _id
        try{
            const response = await products.findOne({[product ? "product":"_id"]:key})
            res.send({ok:true,message:response})
        } catch {
            res.send({ok:false, message:`something went wrong orale`})  
        }
}

const getProductCat = async(req,res) => {
    const{ category } = req.params
        try{
            const response= await products.find({catID:category})
            res.send({ok:true, message:response})
        } catch {
            res.send({ok:false, message:`something went wrong`})  
    }
}

const getProductCat2 = async(req,res) => {
    const{ array } = req.body
        try{
            console.log(array)
            var array2 = array.map(ele => ele.product)
            console.log(array2)
            const response= await products.find({product:{$in:array2}});
            console.log(response,'response')
            res.send({ok:true, message:response})
        } catch {
            res.send({ok:false, message:`something went wrong`})  
    }
}

const update = async(req,res) => {
   const {key1,key2,key3,value1,value2,value3,value4,value5,_id} = req.body

        try{console.log(req.body)
            const response = await products.findOne({_id:_id})
            if (value1 != undefined || value1 != null || value1 != ''){
                var response2 = await products.updateOne({_id:_id},{[key1]:value1,})
            }
            if (value2 == 'false') {
                var response2 = await products.updateOne({_id:_id},{[key2]:false,})
            } 
            if (value2 == 'true') {
                var response2 = await products.updateOne({_id:_id},{[key2]:true,})
            } 
            if (value3 != undefined || value3 != null || value3 != '') {
                var response2 = await products.updateOne({_id:_id},{[key3]:Number(value3),})
            } 
            if (value4 != undefined && value4 != null &&  value4 != '' && value5 != null && value5 != undefined && value5 != '' ) {
                var imageObj = {original: value4, thumbnail: value5}
                var response2 = await products.findOneAndUpdate({_id:_id},{$push:{images:imageObj}})
            } 
            const response3 = await products.findOne({_id:_id})
            console.log(response3)                                                                                                         
            res.send({ok:true,message:response3})
        } catch(error) {
            console.log(error)
            res.send({ok:false, message:`something went wrong`})  
    }
}  

const updateStock = async (req,res,products1) => {
 

         try{
            products1.forEach(async(ele)=>{
                {var response2 = await products.updateOne({product:ele.product},{$inc:{stock:-Math.abs(ele.qty),}})}
            })
            res.send({ok:true,message:`stock updated`})
         } catch(error) {
             console.log(error)
             res.send({ok:false, message:`something went wrong`})  
     }
 }




module.exports = {
    add,
    remove,
    displayProds,
    update,
    getProduct,
    getProductPage,
    getProductCat,
    getProductCat2,
    updateStock,
}