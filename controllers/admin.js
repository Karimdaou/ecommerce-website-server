const admin       = require('../models/admin.js'); 
const bcrypt     = require('bcryptjs');
const jwt        = require('jsonwebtoken');
const config     = require('../config');
const saltRounds = 10;


const register = async (req,res) => {
	const { email , password , password2 } = req.body;
   console.log(req.body)
	if( !email || !password || !password2) return res.json({ok:false,message:'All field are required'});
    if(  password !== password2) return res.json({ok:false,message:'passwords must match'});
    try{
    	const adminx = await admin.findOne({ email })
    	if( adminx ) return res.json({ok:false,message:'email already in use'});
    	const hash = await bcrypt.hash(password, saltRounds)
        console.log('hash =' , hash)
        const newadmin = {
        	email,
        	password : hash
        }
        const create = await admin.create(newadmin)
        res.json({ok:true,message:'successful register'})
    }catch( error ){
        res.json({ok:false,error})
    }
}

const login = async (req,res) => {
    const { email , password } = req.body;
	if( !email || !password ) res.json({ok:false,message:'All field are required'});
	try{
    	const adminx = await admin.findOne({ email });
    	if( !adminx ) return res.json({ok:false,message:'plase provide a valid email'});
        const match = await bcrypt.compare(password, adminx.password);
        if(match) {
           const token = jwt.sign(adminx.toJSON(), config.secret ,{ expiresIn:100080 });
           res.json({ok:true,message:'welcome back',token,email}) 
        }else return res.json({ok:false,message:'invalid password'})
        
    }catch( error ){
    	 res.json({ok:false,error})
    }
}

const verify_token = (req,res) => {
  console.log(req.body.token)
       const { token } = req.body;
       const decoded   = jwt.verify(token, config.secret, (err,succ) => {
             err ? res.json({ok:false,message:'something went wrong'}) : res.json({ok:true,message:'secret page'})
       });      
}

module.exports = { register , login , verify_token }
