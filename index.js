const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    config = require ('./config.js'),
    bodyParser = require('body-parser');
    const cors = require('cors');
// =================== initial settings ===================
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// connnect to mongo

// connecting to mongo and checking if DB is running
async function connecting(){
try {
    await mongoose.connect(`mongodb+srv://kouksdaou:${config.mlabpass}@cluster0-72uea.mongodb.net/test?retryWrites=true&w=majority`, { useUnifiedTopology: true , useNewUrlParser: true })
    console.log('Connected to the DB')
} catch ( error ) {
    console.log('ERROR: Seems like your DB is not running, please start it up !!!');
}
}
connecting()
// temp stuff to suppress internal warning of mongoose which would be updated by them soon
mongoose.set('useCreateIndex', true);
// end of connecting to mongo and checking if DB is running


app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");

	next();
});

// routes
app.use('/products', require('./routes/products'));
app.use('/admin', require('./routes/admin'));
app.use('/emails', require('./routes/emails'));
app.use('/payment', require('./routes/payment'));
// app.use('/update_stock', require('./routes/update_stock'));
// Set the server to listen on port 3000

var PORT = process.env.PORT || 3001
app.listen(PORT, function() {
    console.log(`Serving my master on port ${PORT}!`)
})
