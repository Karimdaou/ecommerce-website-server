const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ordersSchema = new Schema({
    products:{
        type:Array,
        required:true 
        },
    address:{
        type:Object,
        required:true, 
        } 
});
module.exports = mongoose.model('orders', ordersSchema);
