const mongoose = require('mongoose');

const emailsSchema = new mongoose.Schema({
    user:{ type:String, unique:true, required:true },
    pwd:{ type:String, required:true }
});
module.exports = mongoose.model('emails', emailsSchema);

