const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const prodSchema = new Schema({
    product:{
        type : String,
        unique : true,
        requiered : true,
    }, price:{
        type : Number,
        requiered : true,
    }, catID:{
        type : String,
        requiered : true,
    }, color:{
        type : String,
        requiered : true,
    }, onSale:{
        type : Boolean,
        requiered : true,
        default : false,
    }, bestSeller:{
        type : Boolean,
        requiered : true,
        default : false,
    }, images: {
        type: Array,
        required:false
    }, description: {
        type: String,
        requiered: false
    }, image: {
        type: String,
        required: false
    },  stock: {
        type: Number,
        required: false
    }
})
module.exports =  mongoose.model('products', prodSchema);