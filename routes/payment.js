const router = require("express").Router();
const controllers = require("../controllers/payment");

router.post("/charge", controllers.charge);

module.exports = router;
