const router = require('express').Router()
const controller= require('../controllers/products.js')

router.post('/add', controller.add)
router.post('/remove', controller.remove)
router.get('/displayProds', controller.displayProds)
router.post('/update', controller.update)
router.post('/getProduct', controller.getProduct)
router.post('/getProductPage', controller.getProductPage)
router.get('/getProducts/:category', controller.getProductCat)
router.post('/getProducts/category', controller.getProductCat2)
// router.post('/getProducts/checkStock', controller.checkStock)

module.exports = router