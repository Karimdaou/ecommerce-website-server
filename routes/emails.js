const router     = require('express').Router();
const controller = require('../controllers/emails.js')

router.post('/sendEmail',controller.send_email)

module.exports = router
